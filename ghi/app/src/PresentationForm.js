import React from 'react';

class PresentationForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            presenter_name: '',
            presenter_email: '',
            company_name: '',
            title: '',
            synopsis: '',
            conference: '',
            conferences: [],
        };
    }
    async componentDidMount(){
        const url ='http://localhost:8000/api/conferences'
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({conferences: data.conferences})
        }


        this.handleChangeConference = this.handleChangeConference.bind(this);
        this.handleChangeSynopsis = this.handleChangeSynopsis.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleCompanyChange = this.handleCompanyChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handleChangeTitle = this.handleChangeTitle.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        console.log(data);
        delete data.conferences;
        const href = data.conference;
        delete data.conference;

        const presentationUrl = `http://localhost:8000${href}presentations/`
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            },
          };

        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation);
            const cleared = {
                presenter_name: '',
                presenter_email: '',
                company_name: '',
                title: '',
                synopsis: '',
                conference: '',
            }
            this.setState(cleared);
        }
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({presenter_name: value});
    }

    handleEmailChange(event){
        const value = event.target.value;
        this.setState({presenter_email: value});
    }

    handleChangeSynopsis(event){
        const value = event.target.value;
        this.setState({synopsis: value});
    }

    handleCompanyChange(event){
        const value = event.target.value;
        this.setState({company_name: value});
    }

    handleChangeConference(event){
        const value = event.target.value;
        this.setState({conference: value});
    }
    handleChangeTitle(event){
        const value = event.target.value;
        this.setState({title: value});
    }



render() {
  return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create A New Presentation</h1>
            <form onSubmit={this.handleSubmit} id="create-presentation-form">
                <div className="form-floating mb-3">
                    <input onChange={this.handleNameChange} value={this.state.presenter_name} placeholder="Presenter Name" required type="text" id="presenter_name" name="presenter_name" className="form-control" />
                    <label htmlFor="name">Presenter Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={this.handleEmailChange} value={this.state.presenter_email} placeholder="Presenter Email" required type="text" name="presenter_email" id="presenter_email" className="form-control" />
                    <label htmlFor="presenter_email">Presenter Email</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={this.handleCompanyChange} value={this.state.company_name} placeholder="Company Name" required type="text" name="company_name" id="company_name" className="form-control" />
                    <label htmlFor="company_name">Company Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={this.state.title} onChange={this.handleChangeTitle} placeholder="Title" required type="text" name="title" id="title" className="form-control" />
                    <label htmlFor="title">Title</label>
                </div>
                <div className="mb-3">
                    <label htmlFor="synopsis">Synopsis</label>
                    <textarea value={this.state.synopsis} onChange={this.handleChangeSynopsis} className="form-control" placeholder="Enter Synopsis Here" required type="text" name="synopsis" id="synopsis" rows="3"></textarea>
                </div>
                <div className="mb-3">
                  <select value={this.state.conference} onChange={this.handleChangeConference} required name="conference" id="conference" className="form-select">
                    <option value="">Choose A Conference</option>
                    {this.state.conferences.map(conference => {
                        return (
                            <option key={conference.href} value={conference.href}>
                            {conference.name}
                            </option>
                            )
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    );
  }
}
export default PresentationForm;